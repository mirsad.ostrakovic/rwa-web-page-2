import mysql.connector
from decimal import *
from itertools import *
import json
import urllib.request
import string
import random
from math import sqrt


def confidence(ups, downs):
    n = ups + downs

    if n == 0:
        return 0

    z = 1.96 # = 95%
    phat = float(ups) / n
    return ((phat + z*z/(2*n) - z * sqrt((phat*(1-phat)+z*z/(4*n))/n))/(1+z*z/n))


def PreIncrement(var):
	var[0] = var[0] + 1;
	return var[0];



mydb = mysql.connector.connect(
  host="sfet.ba",
  user="sfetba_RWA_user",
  passwd="wIlk?$nv[wtE",
  database="sfetba_RWA"
)

count = 50
API_KEY = 'AIzaSyD-8Rv2NLpVsceAHSQ7kwwV3oAzDUNkUlA'
randomGen = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(3))


#GET DATA FROM YOUTUBE VIA YOUTUBE DATA API
urlData = "https://www.googleapis.com/youtube/v3/search?key={}&maxResults={}&part=snippet&type=video&q={}".format(API_KEY,count,randomGen)
webURL = urllib.request.urlopen(urlData)
data = webURL.read()
encoding = webURL.info().get_content_charset('utf-8')
results = json.loads(data.decode(encoding))



mycursor = mydb.cursor()

#GET SEQUNCE NUMBER FOR VIDEO_DATABASE_ID
mycursor.execute("SELECT * FROM SEQUENCE")
myresult = mycursor.fetchall()
seq_num = 0;
for x in myresult:
  seq_num = Decimal(x[1]);
print("seq_num: ", seq_num)
seq_num = [ seq_num ];



#GENERATE NEW ENTRIES TO ADD INTO DATABASE
newEntries = []

for data in results['items']:
    videoId = (data['id']['videoId']);
    posVotesVideo = random.randint(1,500)
    negVotesVideo = random.randint(1,500)
    ratingVideo = confidence(posVotesVideo, negVotesVideo);
    newEntry = (PreIncrement(seq_num), str(posVotesVideo), str(negVotesVideo), str(ratingVideo), videoId)
    #newEntry = (PreIncrement(seq_num), "2000", "350", "0.7", videoId)
    newEntries.append(newEntry)

print(newEntries)




#INSERT ENTRY INTO DATABASE
sql = "INSERT INTO VIDEO (VIDEO_DATABASE_ID, VIDEO_LIKES, VIDEO_DISLIKES, VIDEO_SCORE, VIDEO_YOUTUBE_ID) VALUES (%s, %s, %s, %s, %s)"

#val = [
# (PreIncrement(seq_num), '1000', '500', '0.7', 'FakeID1' + str(seq_num[0])),
# (PreIncrement(seq_num), '1000', '500', '0.7', 'FakeID2' + str(seq_num[0])),
# (PreIncrement(seq_num), '1000', '500', '0.7', 'FakeID2' + str(seq_num[0]))
#]

mycursor.executemany(sql, newEntries)


#UPDATE SEQUENCE NUMBER
new_seq = seq_num[0]
sql = "UPDATE SEQUENCE SET SEQ_COUNT =" + str(new_seq);
mycursor.execute(sql)


mydb.commit()


